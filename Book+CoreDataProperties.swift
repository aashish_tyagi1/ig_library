//
//  Book+CoreDataProperties.swift
//  MyLibrary
//
//  Created by Ashish on 19/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Book {

    @NSManaged var title: String?
    @NSManaged var author: String?
    @NSManaged var url: String?
    @NSManaged var category: Category?

}
