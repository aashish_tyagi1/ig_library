//
//  DataBaseHelper.swift
//  MyLibrary
//
//  Created by Ashish on 19/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

import UIKit
import CoreData

class DataBaseHelper: NSObject {

    let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
    
    var results = [NSManagedObject]()

    func addNewEnteriesInDataBase(EntityName: String, AttributeName: String, newName: String) -> NSManagedObject{
        
        let manageContext = appDelegate?.managedObjectContext
        
        let entity = NSEntityDescription.entityForName(EntityName, inManagedObjectContext: manageContext!)
        
        let manageObj = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: manageContext)
        manageObj.setValue(newName, forKey: AttributeName)

        do{
            try manageContext!.save()
        }
        catch let error as NSError{
            
            print("Error is -- \(error.localizedDescription)")
            CommonUtils.showAlert(nil, title: "Alert", message: error.localizedDescription)
        }

        return manageObj
    }
    
    func fetchDataFromDataBase( controller: UIViewController,EntityName: String) -> [NSManagedObject] {
        
        let manageContext = appDelegate?.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: EntityName)
        
        do{
        
            let fetchData = try manageContext?.executeFetchRequest(fetchRequest)
            results  = fetchData as! [NSManagedObject]
        }
        catch let error as NSError {
            
            CommonUtils.showAlert(controller, title: "Alert", message: error.localizedDescription)
        } 
        
        return results
}
    
    
    
func InsertIntoDataBase(entityObj: NSManagedObject) -> NSManagedObject{
        
    let manageContext = appDelegate?.managedObjectContext
        do{
            try manageContext!.save()
        }
        catch let error as NSError{
            
            print("Error is -- \(error.localizedDescription)")
            CommonUtils.showAlert(nil, title: "Alert", message: error.localizedDescription)
        }
        return entityObj
    }
    
    
}
