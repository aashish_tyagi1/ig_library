//
//  CommonUtils.swift
//  MyLibrary
//
//  Created by Ashish on 16/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

import UIKit

class CommonUtils: NSObject {

    //--1] Alert *********
    class func showAlert(controller:UIViewController!, title:String, message: String) {
        
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .Alert)
        controller!.presentViewController(alertController, animated: true, completion: nil)
        
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
    }
  
}
