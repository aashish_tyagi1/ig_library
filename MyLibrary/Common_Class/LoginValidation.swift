//
//  LoginValidation.swift
//  MyLibrary
//
//  Created by Ashish on 16/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

import UIKit

class LoginValidation: NSObject {
    
    //--1] Login Validation *********
    class internal func validateLoginDetails(controller: UIViewController, username: String, password: String) -> Bool {
        
        if username.characters.count == 0 {
            
            CommonUtils.showAlert(controller, title: "Alert", message: "Username is empty")
            return false
        }
        else if password.characters.count == 0 {
            
            CommonUtils.showAlert(controller, title: "Alert", message: "Password is empty")
            return false
        }
        else if kUsername != username {
        
            CommonUtils.showAlert(controller, title: "Alert", message: "Your username is incorrect")
            return false
        }
        else if kPassword != password {
        
            CommonUtils.showAlert(controller, title: "Alert", message: "Your password is incorrect")
            return false
        }
        return true
    }
    
}
