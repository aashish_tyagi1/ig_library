//
//  LoginViewController.swift
//  MyLibrary
//
//  Created by Ashish on 19/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
    }
    
    //--1] Submit method Action ************
    @IBAction func submitButtonTapped(sender: AnyObject) {
        
        usernameTextField.text = "aashish"
        passwordTextField.text = "aashish"
        
        if LoginValidation.validateLoginDetails(self,username:usernameTextField.text!, password:passwordTextField.text! ) {
            
            let mainScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MainScreenViewController") as! MainScreenViewController
            self.navigationController?.pushViewController(mainScreen, animated:true)
        }
    }
    
    override func didReceiveMemoryWarning() {
       
        super.didReceiveMemoryWarning()
    }
    
}
