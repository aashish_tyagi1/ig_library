//
//  AddCategoryViewController.swift
//  MyLibrary
//
//  Created by Ashish on 19/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

import UIKit

import CoreData

class AddCategoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var categoryNameTextField: UITextField!
    @IBOutlet var tableView: UITableView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    let dbHelper = DataBaseHelper()
    var tableData = [NSManagedObject]()
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.rowHeight = 40
    }
    
    override func viewWillAppear(animated: Bool) {
        
      tableData = dbHelper.fetchDataFromDataBase(self, EntityName: "Category")
      tableView.reloadData()
    }
    
    // MARK: UITable View Method *********************
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        let category = tableData[indexPath.row]
        cell.textLabel?.text = category.valueForKey("name") as? String
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
    
        let addBookView = self.storyboard?.instantiateViewControllerWithIdentifier("AddBookViewController") as! AddBookViewController
        addBookView.selCategoryObj = tableData[indexPath.row] as? Category
        self.navigationController?.pushViewController(addBookView, animated: true)
        
 }
    
    // MARK: UIButton Clicked Method ****************
    
    @IBAction func submitButtonTapped(sender: AnyObject) {
        
        if categoryNameTextField.text?.characters.count == 0 {
            
            CommonUtils.showAlert(self, title: "Alert", message: "Please enter category name")
        }
        else{
            
        let category = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: appDelegate.managedObjectContext) as! Category
            
          category.name = categoryNameTextField.text
          let addObj = dbHelper.InsertIntoDataBase(category)
          categoryNameTextField.text = ""
          tableData.append(addObj)
          tableView.reloadData()
        }
        
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
}