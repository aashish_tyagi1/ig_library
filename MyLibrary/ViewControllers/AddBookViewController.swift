//
//  AddBookViewController.swift
//  MyLibrary
//
//  Created by Ashish on 19/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

import UIKit

import CoreData

class AddBookViewController: UIViewController {


    @IBOutlet var backButtonTapped: UIButton!
    @IBOutlet var bookTitleTextField: UITextField!
    @IBOutlet var authorName: UITextField!
    @IBOutlet var urlTextField: UITextField!
    
    var selCategoryObj:Category?
    
    let dbHelper = DataBaseHelper()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    // MARK: UIBUtton Clicked Method ****************************
   
    @IBAction func backButtonTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addBookButtonTapped(sender: AnyObject) {
     
        if validateBookDetails(){
            
            let book = NSEntityDescription.insertNewObjectForEntityForName("Book", inManagedObjectContext: appDelegate.managedObjectContext) as! Book
            
            book.title = bookTitleTextField.text
            book.author = authorName.text
            book.url = urlTextField.text
            book.category = selCategoryObj
            dbHelper.InsertIntoDataBase(book)
            
            for vwController in (self.navigationController?.viewControllers)! {
                
                if vwController .isKindOfClass(MainScreenViewController) {
                    
                    self.navigationController?.popToViewController(vwController, animated: true)
                }
            }
        }
  }
    
    
   func validateBookDetails() -> Bool {
        
        if (bookTitleTextField.text?.characters.count == 0 || authorName.text?.characters.count == 0 || urlTextField.text?.characters.count == 0 ) {
            
        CommonUtils.showAlert(self, title: "Alert", message: "All Field value is compulsary")
            return false;
        }
        return true;
    }
    
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }

}
