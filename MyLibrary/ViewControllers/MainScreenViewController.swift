//
//  MainScreenViewController.swift
//  MyLibrary
//
//  Created by Ashish on 19/05/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

import UIKit
import CoreData

class MainScreenViewController: UIViewController,SKSTableViewDelegate {
    
    @IBOutlet var tableView: SKSTableView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let dbHelper = DataBaseHelper()
    var tableData = [NSManagedObject]()
 
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerClass(SKSTableViewCell.self, forCellReuseIdentifier: "cell")

        tableView.rowHeight = 40
        tableView.SKSTableViewDelegate = self
        tableView.shouldExpandOnlyOneCell = true
    }
    
    override func viewWillAppear(animated: Bool) {
        
        tableData = dbHelper.fetchDataFromDataBase(self, EntityName: "Category")
        tableView.refreshData()
        tableView.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: SKSTableView!, numberOfSubRowsAtIndexPath indexPath: NSIndexPath!) -> Int {
        
        let category  = tableData[indexPath.row] as! Category
        let bookArray = category.books!.array
        
        return bookArray.count;
    }
    
    
    func tableView(tableView: SKSTableView!, cellForSubRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        
        var cell:SKSTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")! as! SKSTableViewCell
         cell = SKSTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        
        let category = tableData[indexPath.row] as! Category
        let bookArray = category.books!.array as! [Book]
        
        cell.accessoryView = nil
        cell.textLabel?.text =  bookArray[indexPath.subRow-1].valueForKey("title") as? String
        
        let detailText = bookArray[indexPath.subRow-1].valueForKey("author") as? String
        
        cell.detailTextLabel?.text =  "Author: " + detailText!
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
    }
    

    func tableView(tableView: SKSTableView!, didSelectSubRowAtIndexPath indexPath: NSIndexPath!) {
      
        tableView.collapseCurrentlyExpandedIndexPaths()
    }
    
    // MARK: UITable View Method *********************
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return tableData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell:SKSTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")! as! SKSTableViewCell
        let category = tableData[indexPath.row]
        cell.textLabel?.text = category.valueForKey("name") as? String
        
       // cell.expandable = true
        return cell
    }
    
    func tableView(tableView: SKSTableView!, shouldExpandSubRowsOfCellAtIndexPath indexPath: NSIndexPath!) -> Bool {
        
        return true
    }
    
    // MARK: UIButton Clicked Method ********************
    
    //--1] Back Button
    @IBAction func backButtonTapped(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //--2] Add Category Button
    @IBAction func addCategoryButtonTapped(sender: AnyObject) {
       
        let mainScreen = self.storyboard?.instantiateViewControllerWithIdentifier("AddCategoryViewController") as! AddCategoryViewController
        self.navigationController?.pushViewController(mainScreen, animated:true)
    }
    
    //----Other Method *****************************
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
